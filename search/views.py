import logging
import re
from django.db import OperationalError
from elasticsearch_dsl import Search, Q
from rest_framework import permissions
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from cartolab.pagination import ElasticLimitOffsetPagination
from datasets.bitmaps.models import GroupFilter
from datasets.models import ExtraField, Point
from datasets.serializers import PointSerializer
from datasets.views import PrivateDatasetPermission
from search.search import CustomSearchManager

logger = logging.getLogger(__name__)

POINT_FIELDS = ['id', 'score', 'nature', 'rank',
                'label', 'position', 'url', 'img_data', 'location']
try:
    POINT_FIELDS.extend(
        list(ExtraField.objects.values_list('key', flat=True).distinct())
    )
except OperationalError:
    pass


class PointList(GenericAPIView):
    """
    Lists the points for a given dataset
    """
    permission_classes = (PrivateDatasetPermission,)
    pagination_class = ElasticLimitOffsetPagination
    serializer_class = PointSerializer

    def get(self, request, key, version):
        return self.list()

    def get_queryset(self):
        """Get Queryset."""

        index_name = self.kwargs['key'] + "-" + self.kwargs['version']
        queryset = Search(index=index_name, doc_type=Point)

        return queryset

    def list(self):
        queryset = self.get_points_search()
        extra_point = self.get_custom_search()

        page = self.paginate_queryset(queryset)
        if page is not None:
            self.add_custom_search_result(page, extra_point)
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)

        return Response(serializer.data)

    def add_custom_search_result(self, page, search_result):
        """
        Insert prediction search result as first item
        :param page:
        :param search_result:
        :return:
        """
        if search_result is None or self.paginator is None:
            return
        if self.paginator.offset == 0:
            page.insert(0, search_result)

    def get_custom_search(self):
        search_q = self.request.query_params.get('search', '')

        if search_q and len(search_q.split()) > 3:
            manager = CustomSearchManager()

            return manager.search(self.kwargs['key'], search_q)

        return None

    def get_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        serializer_class = self.get_serializer_class()
        kwargs['context'] = self.get_serializer_context()
        neighbors = self.request.query_params.get('neighbors', 0)

        if neighbors != 0:
            kwargs['expand_neighbors'] = neighbors

        serializer = serializer_class(*args, **kwargs)

        return serializer

    def get_points_search(self):
        """
        Filter the queryset based on the query params
        """
        s = self.get_queryset()
        s = s.sort('-score')

        # Filter by nature
        nature = self.request.query_params.get('nature', None)
        if nature is not None:
            s = s.query(
                Q('bool',
                  should=[Q('term', nature=n) for n in nature.split(',')])
            )

        # Filter by bounding box
        box = self.request.query_params.get('box', '')
        if box:
            coordinates = match_bounding_box(box)
            if coordinates:
                s = s.filter('geo_bounding_box', position=coordinates)

        # Always exclude neighbors, unless the request asks for it specifically
        neighbors = self.request.query_params.get('neighbors', 0)
        source = [] if neighbors == 0 else ['neighbors']
        source.extend(POINT_FIELDS)
        s = s.source(include=source)

        # Text query in label field
        search_q = self.request.query_params.get('search', '')
        if search_q:
            logger.debug(self.request.get_full_path())
            s = s.query('match', label=search_q)
            s = s.sort('_score', '-score')

        for field, value in self.request.query_params.items():
            if (field.startswith(GroupFilter.REF) or
                    field.startswith(GroupFilter.DIS)):

                should = [Q('term', **{field[3:]: n})
                          for n in value.split(',')]
                should.append(
                    Q('bool',
                      should=[Q('term', rank=n) for n in value.split(',')])
                )
                s = s.query(Q('bool', should=should))

            elif (field.startswith(GroupFilter.INT) or
                  field.startswith(GroupFilter.STR)):

                s = s.query(
                    Q('bool',
                      should=[
                          Q('term', **{field[3:]: n}) for n in value.split(',')
                      ])
                )

        return s


def match_bounding_box(query):
    prog = re.compile(r"""\(
                        (-?\d+\.\d+)  # the first x coordinates
                        ,\s?        # comma and optional space
                        (-?\d+\.\d+)  # the first y coordinates
                        \),\s?\(
                        (-?\d+\.\d+)  # the second x coordinates
                        ,\s?        # comma and optional space
                        (-?\d+\.\d+)  # the second y coordinates
                        \)""", re.X)
    groups = re.findall(prog, query)
    if groups:
        (x1, y1, x2, y2) = groups[0]

        return {
            'top_left': {'lat': float(y1), 'lon': float(x1)},
            'bottom_right': {'lat': float(y2), 'lon': float(x2)}
        }

    return {}


class PointNeighborsView(APIView):
    """
    Lists the neighbors for a given point
    """
    permission_classes = (permissions.AllowAny,)
    serializer_class = PointSerializer

    def get(self, request, key, version, pid, nature):
        if pid.startswith('carto-'):
            return self.get_projected_neighbors(key, version, pid, nature)

        return self.get_neighbors(key, version, pid, nature)

    def get_neighbors(self, key, version, pid, nature):
        index_name = key + "-" + version
        point = Point.get(pid, index=index_name)

        for neighbors in point.neighbors:
            if neighbors.nature == nature:
                ids = [
                    {
                        '_id': id,
                        '_source': {'exclude': ['neighbors']}
                    } for id in neighbors.ids
                ]
                points = Point.mget(ids, index=index_name)
                serializer = PointSerializer(points, many=True)

                return Response(serializer.data)

        return Response([])

    def get_projected_neighbors(self, key, version, pid, nature):
        index_name = key + "-" + version
        manager = CustomSearchManager()
        ranks = manager.get_neighbors(index_name, pid, nature)

        if ranks is None:
            return Response([])

        s = Search(index=index_name, doc_type=Point)
        s = s.query('term', nature=nature)
        s = s.query('terms', rank=ranks)
        r = s[:10].execute()

        neighbors = list(r)
        neighbors.sort(key=lambda point: ranks.index(point.rank))

        serializer = PointSerializer(neighbors, many=True)

        return Response(serializer.data)
