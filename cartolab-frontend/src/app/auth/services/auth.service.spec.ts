import { TestBed, inject, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpRequest } from '@angular/common/http';

import { environment } from 'src/environments/environment';

import { User } from '../models/user';
import { AuthService } from './auth.service';

describe('AuthService', () => {
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [AuthService]
    });

    // Inject the http service and test controller for each test
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should be created', inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy();
  }));

  describe('checkIsUserAdmin', () => {
    /**
    * See https://github.com/angular/angular/issues/20690
    */
    it('should send a request to the admin endpoint', async(inject([AuthService], (service: AuthService) => {
      service.checkIsUserAdmin().subscribe((isAdmin: boolean) => expect(isAdmin).toBeTruthy());
      const expectedUrl = environment.apiEndpoint + '/users/isadmin/';

      const req = httpTestingController.expectOne(expectedUrl);
      expect(req.request.method).toEqual('GET');
      req.flush(1);
    })));
  });

  describe('login', () => {
    const fakeToken = {
      'token':
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYmZlYzMxNjctY2RlYy00Y2JkLWJlZTUtZTgyZTN'
        + 'jNDczMGNjIiwidXNlcm5hbWUiOiJhZG1pbkBscmkuZnIiLCJleHAiOjE1MzYzMjQ2NDEsImVtYWlsIjoiYWRtaW5AbHJ'
        + 'pLmZyIn0.od9Xy7ugHmZKS3s4nASTvi_LN2BwCXssHFFNAc07-fI'
    };

    it('should send a request to the login endpoint', async(inject([AuthService], (service: AuthService) => {
      const email = 'admin@lri.fr';
      const password = 'admin';
      service.login(email, password).subscribe((user: User) => {
        expect(user.email).toEqual(email);
      });

      const expectedUrl = environment.apiEndpoint + '/api-token-auth/';
      const req = httpTestingController.expectOne(
        (r: HttpRequest<any>) => r.url === expectedUrl && r.method === 'POST' && JSON.stringify(r.body) === JSON.stringify({
          email: email, password: password
        }));
      req.flush(fakeToken);
    })));


    it('should store the infos in localStorage', async(inject([AuthService], (service: AuthService) => {
      const email = 'admin@lri.fr';
      const password = 'admin';
      service.login(email, password).subscribe();
      const expectedUrl = environment.apiEndpoint + '/api-token-auth/';
      const req = httpTestingController.expectOne(expectedUrl);
      req.flush(fakeToken);

      expect(service.getToken()).toEqual(fakeToken.token);
      expect(service.getUserId()).toEqual('bfec3167-cdec-4cbd-bee5-e82e3c4730cc');
    })));
  });
});
