import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { NgSelectModule } from '@ng-select/ng-select';
import { MatSliderModule } from '@angular/material/slider';
import { of } from 'rxjs';
import { DatasetServiceStub } from 'src/testing/services.stubs';
import { GroupFilterComponent } from './group-filter.component';
import { DatasetService } from 'src/app/datasets/services/dataset.service';
import { Dataset, GroupFilter, FilterValue } from 'src/app/datasets/models';

describe('GroupFilterComponent', () => {
  let component: GroupFilterComponent;
  let fixture: ComponentFixture<GroupFilterComponent>;
  let groupFilter: GroupFilter;
  let dataset: Dataset;
  const datasetServiceStub = new DatasetServiceStub();
  let datasetService: DatasetService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        MatSliderModule,
        NgSelectModule
      ],
      declarations: [GroupFilterComponent],
      providers: [
        { provide: DatasetService, useValue: datasetServiceStub }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupFilterComponent);
    component = fixture.componentInstance;
    groupFilter = { label: 'Lab', key: 'struct', multi: true } as GroupFilter;
    dataset = { id: 2, key: 'hal', label: 'HAL' } as Dataset;
    component.filter = groupFilter;
    component.dataset = dataset;
    fixture.detectChanges();
    datasetService = TestBed.get(DatasetService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should search for filter values on input', fakeAsync(() => {
    const values: FilterValue[] = [{ field: 'struct', value: 'CNRS', bitmap_index: 12 }] as FilterValue[];
    (<jasmine.Spy>datasetService.searchFilterValues).and.returnValue(of(values));
    triggerInputChange(fixture, 'cnr');
    tick(300);

    expect(datasetService.searchFilterValues).toHaveBeenCalledWith(dataset, groupFilter.key, 'cnr', '40');
    component.filterValues$.subscribe(result => expect(result).toBe(values));
  }));
});

function triggerInputChange(fixture: ComponentFixture<any>, value: string): void {
  const input = fixture.debugElement.query(By.css('input')).nativeElement;
  input.value = value;
  input.dispatchEvent(new Event('input'));
  fixture.detectChanges();
}

