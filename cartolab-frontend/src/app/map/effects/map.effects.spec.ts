import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { NgZone } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { MainComponent, MaintenanceComponent, DashboardComponent, AboutComponent } from 'src/testing/components.stubs';
import { MapEffects } from './map.effects';
import { DatasetsStore } from 'src/app/datasets/store';
import { Dataset } from 'src/app/datasets/models';
import { MapStore } from 'src/app/map/store';
import { MapService } from 'src/app/map/services';
import { MapServiceStub } from 'src/testing/services.stubs';
import { MapHistory, MapState } from 'src/app/map/history';
describe('MapEffects', () => {

    let router: Router;
    let dsStore: DatasetsStore;
    let mapStore: MapStore;
    let mapService: MapService;
    let mapHistory: MapHistory;
    let ngZone: NgZone;
    const dataset = { id: 1, label: 'Inria', key: 'inria', version: '1.0.0',
		      description: 'Desc', natures: [] } as Dataset;

    beforeEach(async() => {
	TestBed.configureTestingModule({
	    declarations: [
		MainComponent,
		MaintenanceComponent,
		DashboardComponent,
		AboutComponent
	    ],
	    imports: [
		RouterTestingModule.withRoutes([
		    { path: 'maintenance', component: MaintenanceComponent },
		    {
			path: '',
			component: MainComponent,
			children: [
			    { path: 'about', component: AboutComponent },
			    { path: 'map/:key/:version', component: DashboardComponent },
			    { path: '', redirectTo: '/map/arxiv/1.0.0', pathMatch: 'full' },
			]
		    }
		])
	    ],
	    providers: [
		{ provide: MapService, useClass: MapServiceStub },
	    ]
	});
	router = TestBed.get(Router);
	dsStore = TestBed.get(DatasetsStore);
	mapStore = TestBed.get(MapStore);
	mapService = TestBed.get(MapService);
	mapHistory = TestBed.get(MapHistory);
	ngZone = TestBed.get(NgZone);
	TestBed.get(MapEffects);
	ngZone.run(() => router.initialNavigation());
    });
    

    describe('datasetGuard$', () => {
	let setDatasetSpy: jasmine.Spy;

	beforeEach(() => {
	    setDatasetSpy = spyOn(dsStore, 'setDataset');
	});

	it('should listen to navigationEnd events and set the dataset', fakeAsync(() => {
	    ngZone.run(() => router.navigate(['/about']));
	    tick();
	    expect(setDatasetSpy).toHaveBeenCalledWith(null, null);
	    
	    ngZone.run(() => router.navigate(['/map/wikipedia/1.0.0']));
	    tick();
	    expect(setDatasetSpy).toHaveBeenCalledWith('wikipedia', '1.0.0');
	}));
    });

    describe('datasetUpdate$', () => {
	let setDatasetSpy: jasmine.Spy;
	let historySpy: jasmine.Spy;
	
	beforeEach(() => {
	    setDatasetSpy = spyOn(mapStore, 'setDataset');
	    historySpy = spyOn(mapHistory, 'resetHistory');
	});
	
	it('should listen for dataset changes and set it on MapStore and MapService', () => {
	    dsStore.setDatasets([dataset]);
	    dsStore.setDataset(dataset.key, dataset.version);

	    expect(mapService.resetService).toHaveBeenCalled();
	    expect(setDatasetSpy).toHaveBeenCalledWith(dataset, true);
	    expect(historySpy).toHaveBeenCalled();
	});
	
	it('should check if datasetChange is from History', () => {
	    mapHistory.present = new MapState(dataset.key, dataset.version);
	    dsStore.setDatasets([dataset]);
	    dsStore.setDataset(dataset.key, dataset.version);

	    expect(mapService.resetService).toHaveBeenCalled();
	    expect(setDatasetSpy).toHaveBeenCalledWith(dataset, false);
	    expect(historySpy.calls.count()).toBe(0);
	});
    });
    
});

