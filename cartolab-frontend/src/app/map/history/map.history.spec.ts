import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Location } from '@angular/common';
import { DatasetsStore } from 'src/app/datasets/store';
import { Dataset } from 'src/app/datasets/models';
import { MapStore } from 'src/app/map/store';
import { MapHistory, MapState } from 'src/app/map/history';
import { Point } from 'src/app/map/models';

describe('MapHistory', () => {

    let location: Location;
    let dsStore: DatasetsStore;
    let mapStore: MapStore;
    const dataset = { id: 1, label: 'Inria', key: 'inria', version: '1.0.0',
		      current: '1.0.0', description: 'Desc', natures: [] } as Dataset;
    const points = [{ id: '1', score: 10, position: [1.0, 2.0], rank: 0 },
		    { id: '2', score: 5, position: [-5.2, -3.0], rank: 1 }] as Point[];

    beforeEach(() => {
	TestBed.configureTestingModule({
	    imports: [
		RouterTestingModule.withRoutes([])
	    ],
	});
	location = TestBed.get(Location);
	dsStore = TestBed.get(DatasetsStore);
	mapStore = TestBed.get(MapStore);
	TestBed.get(MapHistory);
    });

    describe('saveState', () => {
	let goSpy: jasmine.Spy;
	
	beforeEach(() => {
	    goSpy = spyOn(location, 'go');
	});
	
	it('should save state when selectedPoint changes', () => {
	    dsStore.setDatasets([dataset]);
	    dsStore.setDataset(dataset.key, dataset.version);
	    const state = new MapState(dataset.key, dataset.version,
				       points[1]);

	    mapStore.setSelectedPoint(points[1]);
	    expect(goSpy).toHaveBeenCalledWith('', '', {'mapState': state});
	});

	it('should save state when searchPoints changes', () => {
	    dsStore.setDatasets([dataset]);
	    dsStore.setDataset(dataset.key, dataset.version);
	    const state = new MapState(dataset.key, dataset.version,
				       null, points, 'Search results');

	    mapStore.setSearchPoints(points, 'Search results');
	    expect(goSpy).toHaveBeenCalledWith('', '', {'mapState': state});
	});

	it('should not save state when dataset has not been set', () => {
	    mapStore.setSelectedPoint(points[1]);
	    expect(goSpy.calls.count()).toBe(0);
	});
    });
    
    describe('setStoreState', () => {
	let selectSpy: jasmine.Spy;
	let searchSpy: jasmine.Spy;
	
	beforeEach(() => {
	    selectSpy = spyOn(mapStore, 'setSelectedPoint');
	    searchSpy = spyOn(mapStore, 'setSearchPoints');
	});
	
	it('should set state on mapStore from Location history', () => {
	    const state = new MapState(dataset.key, dataset.version,
				       points[1], points, 'Search results');
	    location.go('/map/hal', '', {'mapState': state});
	    location.go('/about', '', {'randomStuff': true});
	    location.go('/login', '', {});
	    
	    // Go back to /about, should not set state
	    location.back();
	    expect(selectSpy.calls.count()).toBe(0);
	    expect(searchSpy.calls.count()).toBe(0);
	    
	    // Go back to /map/hal, should set state
	    location.back();
	    expect(selectSpy).toHaveBeenCalledWith(points[1]);
	    expect(searchSpy).toHaveBeenCalledWith(points, 'Search results');
	});
    });
});
