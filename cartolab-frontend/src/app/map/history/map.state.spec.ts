import { MapState } from './map.state';
import { Point } from 'src/app/map/models';

describe('MapState', () => {

  let state: MapState;
  let other: MapState;
  const points = [
    { id: 'abc-12-df', score: 10, position: [1.0, 2.0], label: 'Article', nature: 'articles' },
    { id: 'deg-345-gh', score: 5, position: [-5.2, -3.0], label: 'Author', nature: 'authors' },
    { id: 'uio-34-rzh', score: 40, position: [12.5, 6.8], label: 'Team', nature: 'teams' },
    { id: 'tio-789-dfj', score: 50, position: [5.9, -12.8], label: 'Lab', nature: 'labs' }
  ] as Point[];
  beforeEach(() => {
    state = new MapState();
    other = new MapState();
  });


  describe('equal', () => {
    it('should return true if same object', () => {
      expect(MapState.equal(state, state)).toBe(true);
      expect(MapState.equal(null, null)).toBe(true);
      expect(MapState.equal(state, undefined)).toBe(false);
      expect(MapState.equal(null, state)).toBe(false);
    });

    it('should check for selectPoint id equality', () => {
      other.selectedPoint = points[0];
      expect(MapState.equal(state, other)).toBe(false);
      state.selectedPoint = points[1];
      expect(MapState.equal(state, other)).toBe(false);
      other.selectedPoint = points[1];
      expect(MapState.equal(state, other)).toBe(true);
    });

    it('should check for searchTitle equality', () => {
      other.searchTitle = 'Hello';
      expect(MapState.equal(state, other)).toBe(false);
      state.searchTitle = 'World';
      expect(MapState.equal(state, other)).toBe(false);
      other.searchTitle = 'World';
      expect(MapState.equal(state, other)).toBe(true);
    });

    it('should check for searchPoints equality', () => {
      other.searchPoints = points;
      expect(MapState.equal(state, other)).toBe(false);
      state.searchPoints = [{ id: '102', score: 10, position: [12, 15] }] as Point[];
      expect(MapState.equal(state, other)).toBe(false);
      state.searchPoints = points.map(p => ({ id: p.id, position: [-5, 12], score: 10, label: 'Some Point' } as Point));
      expect(MapState.equal(state, other)).toBe(true);
    });
  });

});
