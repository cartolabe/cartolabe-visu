import { Location } from '@angular/common';
import { Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { color } from 'd3-color';
import { select, selectAll, Selection, EnterElement } from 'd3-selection';
import { tile } from 'd3-tile';
import { ZoomTransform } from 'd3-zoom';
import { Subscription, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Dataset, Nature, Group } from 'src/app/datasets/models';
import { DatasetsStore } from 'src/app/datasets/store';
import { MapStore } from 'src/app/map/store';
import { MapProperties } from 'src/app/map/models';
import { MapService } from 'src/app/map/services';

@Component({
  selector: 'app-map-tiles',
  templateUrl: './map-tiles.component.html',
  styleUrls: ['./map-tiles.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MapTilesComponent implements OnInit, OnDestroy {

  @Input() props: MapProperties;
  private tilesUrl = environment.staticEndpoint + '/tiles/';
  private groupTilesUrl = environment.staticEndpoint + '/grouptiles/';
  private svg: Selection<SVGElement, {}, HTMLElement, any>;
  private tile: any;
  private rasterLayers: Selection<SVGElement, Nature, SVGElement, any>;
  private groupLayers: Selection<SVGElement, Group, SVGElement, any>;
  private groupTileLayers: Selection<SVGElement, Nature, SVGElement, Group>;
  private tiles: any;
  private datasetKey: string;
  private datasetVersion: string;
  private subscriptions: Subscription;
  private debouncedZoom$ = new Subject<ZoomTransform>();
  tiling = false;
  private groupVisible = false;

  constructor(private store: DatasetsStore, private mapStore: MapStore,
		private location: Location, private mapService: MapService) {
  }

  ngOnInit() {
    this.subscriptions = this.store.dataset$.subscribe((dataset: Dataset) => {
	this.datasetKey = dataset ? dataset.key : null;
	this.datasetVersion = dataset ? dataset.current : null;
    });
    this.subscriptions.add(this.store.layers$.subscribe(_ => this.setupLayers()));
    this.subscriptions.add(this.debouncedZoom$.pipe(
      debounceTime(300),
      distinctUntilChanged()
    ).subscribe(_ => this.getNewTiles()));
    this.subscriptions.add(
      this.mapStore.groups$.subscribe((groups) => {
        this.groupVisible = groups.length > 0;
        this.drawGroupLayers();
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  initSvg() {
    this.tile = tile().size([this.props.width, this.props.height]);
    this.svg = select<SVGElement, {}>('svg.tiles-svg')
      .attr('preserveAspectRatio', 'none');
    this.setupLayers();
  }

  private setupLayers() {
    this.createFeColorFilters();
    this.drawLayers();
    this.drawGroupLayers();
  }

  drawLayers() {
    if (this.svg) {
      this.rasterLayers = this.svg.select<SVGElement>('g.tile-layers')
        .selectAll<SVGElement, Nature>('g.tile-layer')
        .data(this.store.layers.filter(d => !d.inactive), (d: Nature) => d.id.toString())
        .join(
          enter => enter.append<SVGElement>('g').attr('class', d => d.key + '-layer')
            .classed('tile-layer blend-multiply', true),
          update => update,
          exit => exit.remove()
        );
      this.styleTileLayers();
    }
    this.tileLayers();
  }

  styleTileLayers() {
    this.svg.select<SVGElement>('g.tile-layers')
      .style('opacity', _ => this.groupVisible ? 0.2 : 1)
      .selectAll<SVGElement, Nature>('g.tile-layer')
      .attr('filter', d => this.groupVisible ? '' : 'url(' + this.location.path() + '#' + d.key + 'filter)');
  }

  drawGroupLayers() {
    if (this.svg) {
      this.groupLayers = this.svg.select<SVGElement>('g.group-layers')
        .selectAll<SVGElement, Group>('g.group-layer')
        .data(this.mapStore.groups, (d: Group) => d.uuid)
        .join(
          enter => enter.append<SVGElement>('g').attr('id', (d: Group) => 'g' + d.uuid).classed('group-layer', true)
        );
      this.groupTileLayers = this.groupLayers
        .selectAll<SVGElement, Nature>('g.group-tile-layer')
        .data(this.store.layers.filter(d => !d.inactive), (d: Nature) => d.id.toString())
        .join(
          enter => enter.append<SVGElement>('g').attr('class', d => d.key + '-layer')
            .classed('group-tile-layer blend-multiply', true)
            .attr('filter', d => 'url(' + this.location.path() + '#' + d.key + 'filter-saturate)'),
          update => update,
          exit => exit.remove()
        );
      this.styleTileLayers();
    }
    this.tileGroupLayers();
    this.getNewTiles();
  }

  zoomed(transform: ZoomTransform) {
    const t = transform.translate(this.props.width / 2, this.props.height / 2).scale(512);
    this.tiles = this.tile.scale(t.k).translate([t.x, t.y])();
    this.tileLayers();
    this.tileGroupLayers();
    this.debouncedZoom$.next(transform);
  }

  private tileLayers() {
    if (this.rasterLayers && this.tiles) {
      this.rasterLayers.attr('transform', stringify(this.tiles.scale, this.tiles.translate));
      this.rasterLayers.each((raster: Nature, i: number, groups: SVGElement[]) => {
        const images = select<SVGElement,Nature>(groups[i]).selectAll<SVGElement, any>('image')
	  .data(this.tiles, (d) => [256 * d[0], 256 * d[1], d[2]].join());
        images.join('image')
          .attr('xlink:href', (d) => this.buildTileUrl(raster.key, d[2], d[0], d[1]))
          .attr('x', (d) => 256 * d[0])
          .attr('y', (d) => 256 * d[1])
          .attr('width', 256)
          .attr('height', 256);
      });
    }
  }

  private tileGroupLayers() {
    if (this.groupLayers && this.tiles) {
      this.groupLayers.attr('transform', stringify(this.tiles.scale, this.tiles.translate));
      this.groupTileLayers.each((_: Nature, i: number, groups: SVGElement[]) => {
        const images = select(groups[i]).selectAll('image').data(this.tiles, (d) => [256 * d[0], 256 * d[1], d[2]].join());
        images.join('image')
          .attr('x', (d) => 256 * d[0])
          .attr('y', (d) => 256 * d[1])
          .attr('width', 256)
          .attr('height', 256)
          .on('error', (event: any) => select(event.currentTarget).attr('xlink:href', 'assets/blank.png'));
      });
    }
  }

  private getNewTiles() {
    if (this.groupLayers && this.tiles) {
      this.groupLayers.each((group: Group) => {
        this.tiling = true;
        const z = this.tiles[0][2], x = this.tiles[0][0], y = this.tiles[0][1],
          xend = this.tiles[this.tiles.length - 1][0], yend = this.tiles[this.tiles.length - 1][1];
        this.mapService.requestGroupTile(group, z, x, y, xend, yend, this.store.layers.filter(d => !d.inactive))
          .pipe(tap(() => this.tiling = false))
          .subscribe((tilesMask) => {
            for (const [layer, mask] of Object.entries(tilesMask)) {
              this.svg.select('#g' + group.uuid).select('g.' + layer + '-layer').selectAll('image')
                .attr('xlink:href', (d) => {
                  if (mask[d[0] - x][d[1] - y] === 1.0) {
                    return this.buildGroupTileUrl(group.uuid, layer, d[2], d[0], d[1]);
                  }
                  return '';
                });
            }
          }, () => this.tiling = false);
      });
    }
  }

  private buildTileUrl(layerKey: string, zoom: number, x: number, y: number) {
    return `${this.tilesUrl}${this.datasetKey}/${this.datasetVersion}/${layerKey}/${zoom}/${x}/${y}.png`;
  }

  private buildGroupTileUrl(groupUuid: string, layer: string, zoom: number, x: number, y: number) {
    return `${this.groupTilesUrl}${groupUuid}/${layer}/${zoom}/${x}/${y}.png`;
  }

  setDimedMode(mode: boolean) {
    this.svg.classed('dimed', mode);
  }

  private createFeColorFilters() {
    if (this.svg) {
      this.svg.select<SVGElement>('defs').selectAll<SVGElement, Nature>('g.layer-filter')
        .data(this.store.layers, (d: Nature) => d.id.toString())
        .join(
          enter => appendFeColorFilters(enter),
          update => update,
          exit => exit.remove()
        );
    }
  }
}

function appendFeColorFilters(enter: Selection<EnterElement, Nature, SVGElement, {}>) {
  const sel = enter.append('g').classed('layer-filter', true);
  styleFeColorFilter(sel.append('filter'), false);
  styleFeColorFilter(sel.append('filter'), true);
  return sel;
}

function styleFeColorFilter(filter: Selection<SVGElement, Nature, SVGElement, {}>, saturate: boolean) {
  filter.attr('id', (d: Nature) => d.key + 'filter' + (saturate ? '-saturate' : ''))
    .attr('color-interpolation-filters', 'sRGB')
    .attr('x', 0).attr('y', 0).attr('height', '100%').attr('width', '100%')
    .append('feColorMatrix').attr('type', 'matrix')
    .attr('values', (d: Nature) => {
      let c = color(d.color).rgb();
      if (!saturate) {
        c = c.brighter(1);
      }
  return `0 0 0 0 ${(c.r / 255).toFixed(2)}
          0 0 0 0 ${(c.g / 255).toFixed(2)} 
          0 0 0 0 ${(c.b / 255).toFixed(2)} 
          1 0 0 0 0`;
    });
}

function stringify(scale: number, translate: number[]) {
  const k = scale / 256;
  const r = scale % 1 ? Number : Math.round;
  return 'translate(' + r(translate[0] * scale) + ',' + r(translate[1] * scale) + ') scale(' + k + ')';
}

interface Tile {
  x: number;
  y: number;
  z: number;
  tx: number;
  ty: number;
}
