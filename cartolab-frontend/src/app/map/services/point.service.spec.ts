import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpRequest } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { PointService } from './point.service';
import { Point } from '../models/point';
import { Dataset, Group, Nature, FilterValue } from 'src/app/datasets/models';

describe('PointService', () => {
    let httpTestingController: HttpTestingController;
    let mockDataset: Dataset;
    let mockPoints: Point[];
    let service: PointService;
    const group: Group = { uuid: 'cd865e3d-c00d-4f3d-a999-342927a6e810',
			   bitmaps: '34|56&23', query: '' };
    const dataset = {
	id: 1, key: 'inria', label: 'INRIA', version: '1.0.0',
	current: '1.0.0',
	filters: [
	    { label: 'Year', key: 'year', type: 'IN', multi: false },
	    { label: 'Lab', key: 'labs', type: 'ID', multi: true },
      { label: 'Author', key: 'author', type: 'ID', multi: true }
	]
    } as Dataset;

    beforeEach(() => {
	TestBed.configureTestingModule({
	    imports: [
		HttpClientTestingModule
	    ],
	    providers: [PointService]
	});
	
	httpTestingController = TestBed.get(HttpTestingController);
	service = TestBed.get(PointService);
	mockDataset = { id: 1, label: 'Inria', key: 'inria', version: '1.0.0',
			current: '1.0.0', description: 'Desc' } as Dataset;
	mockPoints = [{ id: '1', score: 10, position: [1.0, 2.0] },
		      { id: '2', score: 5, position: [-5.2, -3.0] }] as Point[];
    });

    afterEach(() => {
	// After every test, assert that there are no more pending requests.
	httpTestingController.verify();
    });
    
    it('should be created', () => {
	expect(service).toBeTruthy();
    });

    describe('fetchPoints', () => {
	it('should send a request with the correct params', () => {
	    let results: Point[];
	    service.fetchPoints(
		mockDataset, 'articles', '20', null, null, null
	    ).subscribe(points => results = points);
	    
	    const expectedUrl = environment.apiEndpoint + '/datasets/' +
		mockDataset.key + '-' + mockDataset.version + '/points/';
	    const req = httpTestingController.expectOne(
		(r: HttpRequest<any>) => r.url.match(expectedUrl) &&
		    r.method === 'GET'
	    );
	    expect(req.request.urlWithParams).toEqual(
		expectedUrl + '?nature=articles&limit=20'
	    );
	    req.flush(mockPoints);
	    expect(results).toEqual(mockPoints);
	});
	
	it('should set the box param if xrange and yrange are defined', () => {
	    const bounds = [[1.3, 2.3], [-3.2, -4.5]];
	    let results: Point[];
	    service.fetchPoints(
		mockDataset, 'words', '10', bounds[0], bounds[1], null
	    ).subscribe(points => results = points);
	    
	    const expectedUrl = environment.apiEndpoint + '/datasets/' +
		mockDataset.key + '-' + mockDataset.version + '/points/';
	    const req = httpTestingController.expectOne(
		(r: HttpRequest<any>) => r.url.match(expectedUrl) &&
		    r.method === 'GET'
	    );
	    expect(req.request.urlWithParams).toEqual(
		expectedUrl + '?box=(1.3,-3.2),(2.3,-4.5)&nature=words&limit=10'
	    );
	    req.flush(mockPoints);
	    expect(results).toEqual(mockPoints);
	});
    });

    describe('getNeighbors', () => {
	it('should send a request with the correct params', () => {
	    let results: Point[];
	    service.getNeighbors(
		mockDataset, mockPoints[0], 'teams'
	    ).subscribe(points => results = points);
	    
	    const expectedUrl = environment.apiEndpoint + '/datasets/' +
		mockDataset.key + '-' + mockDataset.version +
		'/points/' + mockPoints[0].id + '/teams/';
	    const req = httpTestingController.expectOne(
		(r: HttpRequest<any>) => r.url.match(expectedUrl) &&
		    r.method === 'GET'
	    );
	    req.flush(mockPoints);
	    expect(results).toEqual(mockPoints);
	});
    });
    
    describe('requestGroupTile', () => {
	it('should request group tiling', () => {
	    const tiles = { 'articles': [[0, 0, 1], [1, 1, 0]],
			    'authors': [[1, 0, 1], [0, 1, 1]] };
	    const layers = [{ label: 'Authors', key: 'authors' },
			    { label: 'Articles', key: 'articles' }] as Nature[];
	    let results: {[key: string]: number[][]};
	    service.requestGroupTile(
		group, 3, 2, 7, 5, 8, layers
	    ).subscribe(r => results = r);
	    
	    const expectedUrl = environment.apiEndpoint +
		'/groups/' + group.uuid + '/3/2/7/5/8/';
	    const req = httpTestingController.expectOne(
		(r: HttpRequest<any>) => r.url.match(expectedUrl) &&
		    r.method === 'GET'
	    );
	    expect(req.request.urlWithParams).toEqual(
		expectedUrl + '?layers=authors,articles'
	    );
	    req.flush(tiles);
	    expect(results).toEqual(tiles);
	});
    });

    describe('getDatasetGroup', () => {
	it('should get group from backend', () => {
	    const filters = [{ field: 'labs', value: 'CNRS',
			       bitmap_index: 14, rank: 23786, location: '' }];
	    let result: Group;
	    service.getDatasetGroup(dataset, filters).subscribe(g => result = g);
	    
	    const expectedUrl = environment.apiEndpoint + '/datasets/' +
		dataset.key + '-' + dataset.version + '/group/';
	    const req = httpTestingController.expectOne(
		(r: HttpRequest<any>) => r.url.match(expectedUrl) &&
		    r.method === 'GET'
	    );
	    expect(req.request.urlWithParams).toEqual(
		expectedUrl + '?bitmaps=14&query=ID_labs=23786'
	    );
	    req.flush(group);
	    expect(result).toBe(group);
	});

	it('should build bitmaps list correctly', () => {
	    const filters = [{ field: 'labs', value: 'CNRS', bitmap_index: 14, rank: 23786 },
			     [{ field: 'year', value: '2010', bitmap_index: 12 }, { field: 'year', value: '2008', bitmap_index: 10 }],
			     [{ field: 'author', value: 'Jean', bitmap_index: 45, rank: 145 },
			      { field: 'author', value: 'Damien', bitmap_index: 150, rank: 124 }]] as Array<FilterValue | FilterValue[]>;
	    let result: Group;
	    service.getDatasetGroup(dataset, filters).subscribe(g => result = g);
	    
	    const expectedUrl = environment.apiEndpoint + '/datasets/' +
		dataset.key + '-' + dataset.version + '/group/';
	    const req = httpTestingController.expectOne(
		(r: HttpRequest<any>) => r.url.match(expectedUrl) &&
		    r.method === 'GET'
	    );
	    expect(req.request.urlWithParams).toEqual(
		expectedUrl + '?bitmaps=' +
		    encodeURIComponent('14&10|12&45|150')
		    + '&query=ID_labs=23786%26IN_year=2010,2008%26ID_author=145,124');
	    req.flush(group);
	    expect(result).toBe(group);
	});
    });

    describe('getGroupByReference', () => {
	it('should get group from backend', () => {
	    let result: Group;
	    service.getGroupByReference(
		dataset, dataset.filters[1], 126794
	    ).subscribe(g => result = g);
	    
	    const expectedUrl = environment.apiEndpoint + '/datasets/' +
		dataset.key + '-' + dataset.version + '/groupbyreference/';
	    const req = httpTestingController.expectOne(
		(r: HttpRequest<any>) => r.url.match(expectedUrl) &&
		    r.method === 'GET'
	    );
	    expect(req.request.urlWithParams).toEqual
	    (expectedUrl + '?field=labs&rank=' + 126794 + '&query=ID_labs=126794'
	    );
	    req.flush(group);
	    expect(result).toBe(group);
	});
    });
});
