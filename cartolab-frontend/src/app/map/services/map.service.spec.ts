import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { of } from 'rxjs';
import { PointServiceStub } from 'src/testing/services.stubs';
import { MapService } from './map.service';
import { MapStore } from 'src/app/map/store';
import { DatasetsStore } from 'src/app/datasets/store';
import { PointService } from './point.service';
import { Point, MapViewboxBounds } from '../models';
import { Dataset, Nature, Group, GroupFilter } from 'src/app/datasets/models';

describe('MapService', () => {
    let pointService: PointService;
    let mapStore: MapStore;
    let dsStore: DatasetsStore;
    let service: MapService;
    
    let mockDataset: Dataset;
    let mockFilters: Array<Nature>;
    let mockPoints: Point[];
    const group: Group = { uuid: 'cd865e3d-c00d-4f3d-a999-342927a6e810', bitmaps: '34|56&23', query: '' };

    beforeEach(() => {
	TestBed.configureTestingModule({
	    providers: [
		MapService,
		{ provide: PointService, useClass: PointServiceStub },
	    ]
	});

	mockFilters = [
	    {
		id: 101, label: 'Articles', key: 'articles',
		checked: true, limit: 2, color: '#0288d1', icon: '',
		tooltip: '', layer: true, inactive: false,
		layer_tooltip: '', extra_fields: []
	    },
	    {
		id: 102, label: 'Authors', key: 'authors',
		checked: true, limit: 30, color: '#e64a19', icon: '',
		tooltip: '', layer: true, inactive: false,
		layer_tooltip: '', extra_fields: []
	    },
	    {
		id: 103, label: 'Teams', key: 'teams',
		checked: true, limit: 30, color: '#388e3c', icon: '',
		tooltip: '', layer: false, inactive: false,
		layer_tooltip: '', extra_fields: []
	    },
	    {
		id: 104, label: 'Labs', key: 'labs',
		checked: true, limit: 0, color: '#c67100', icon: '',
		tooltip: '', layer: false, inactive: false,
		layer_tooltip: '', extra_fields: []
	    }
	];
	
	mockDataset = {
	    id: 1, label: 'Inria', key: 'inria', version: '1.0.0',
	    current: '1.0.0', description: 'Desc', natures: mockFilters
	} as Dataset;
	mockPoints = [{ id: '1', score: 10, position: [1.0, 2.0], rank: 0 },
		      { id: '2', score: 5, position: [-5.2, -3.0], rank: 1 }] as Point[];

	pointService = TestBed.get(PointService);
	service = TestBed.get(MapService);
	mapStore = TestBed.get(MapStore);
	dsStore = TestBed.get(DatasetsStore);
    });

    it('should be created', () => {
	expect(service).toBeTruthy();
    });

    describe('searchInBounds', () => {
	it('should not search if dataset is not set', () => {
	    const bounds = new MapViewboxBounds([], []);
	    service.searchInBounds(bounds);
	    expect((<jasmine.Spy>pointService.fetchPoints).calls.any()).toBe(false);
	});

	it('should search for points within bounds and push it to searchPoints$', () => {
	    const bounds = new MapViewboxBounds([], []);
	    dsStore.setDatasets([mockDataset]);
	    dsStore.setDataset(mockDataset.key, mockDataset.version);
	    mapStore.setDataset(mockDataset);
	    (<jasmine.Spy>pointService.fetchPoints).and.returnValue(of(mockPoints));
	    const qsSpy = spyOnProperty(mapStore,
					'groupQueryString').and.returnValue('RE_labs=1234');
	    const setSearchSpy = spyOn(mapStore, 'setSearchPoints');

	    service.searchInBounds(bounds);
	    const expectedNatures = 'articles,authors,teams';
	    expect(pointService.fetchPoints)
		.toHaveBeenCalledWith(mockDataset, expectedNatures, '10',
				      bounds.xrange, bounds.yrange, null, 'RE_labs=1234');
	    expect(setSearchSpy).toHaveBeenCalledWith(mockPoints,
						      'Highest ranked points in search zone');
	    expect(qsSpy).toHaveBeenCalled();
	});
    });

    describe('updateViewBox', () => {
	let spy: jasmine.Spy;
	beforeEach(() => {
	    spy = spyOn(mapStore, 'addViewBoxPoints');
	});

	it('should return an empty array if dataset is not set', () => {
	    service.updateViewBox(new MapViewboxBounds([], []), false);
	    expect((<jasmine.Spy>pointService.fetchPoints).calls.any()).toBe(false);
	    expect(spy).toHaveBeenCalledWith([]);
	});

	it('should fetch points for each filter nature', () => {
	    dsStore.setDatasets([mockDataset]);
	    dsStore.setDataset(mockDataset.key, mockDataset.version);
	    mapStore.setDataset(mockDataset);
	    (<jasmine.Spy>pointService.fetchPoints).and.returnValue(of(mockPoints));
	    spyOnProperty(mapStore, 'groupQueryString').and.returnValue('RE_labs=1234');
	    service.updateViewBox(new MapViewboxBounds([], []), false);
	    
	    // We expect 3 requests, one for each nature checked with
	    // limit > 0 in the filters
	    mockFilters.forEach((filter, i) => {
		if (filter.checked && filter.limit > 0) {
		    expect((<jasmine.Spy>pointService.fetchPoints).calls.argsFor(i))
			.toEqual([mockDataset, filter.key,
				  String(filter.limit), [], [], null, 'RE_labs=1234']);
		}
	    });
	});

	it('should store labels on mapStore', () => {
	    // Only send requests for articles, authors & teams
	    mockFilters[3].checked = false;
	    dsStore.setDatasets([mockDataset]);
	    dsStore.setDataset(mockDataset.key, mockDataset.version);
	    mapStore.setDataset(mockDataset);
	    (<jasmine.Spy>pointService.fetchPoints).and.callFake(fakeFetchPoints);
	    
	    service.updateViewBox(new MapViewboxBounds([], []), false);
	    expect((<jasmine.Spy>pointService.fetchPoints).calls.count()).toBe(3);
	    expect(spy.calls.count()).toBe(3);
	    expect(spy.calls.argsFor(0)).toEqual([fakePoints['articles']]);
	    expect(spy.calls.argsFor(1)).toEqual([fakePoints['authors']]);
	    expect(spy.calls.argsFor(2)).toEqual([fakePoints['teams']]);
	});
    });

    describe('typeaheadPoints$', () => {
	it('should search for points when an input is entered', fakeAsync(() => {
	    dsStore.setDatasets([mockDataset]);
	    dsStore.setDataset(mockDataset.key, mockDataset.version);
	    mapStore.setDataset(mockDataset);
	    const query = 'what';
	    let results: Point[];
	    service.typeaheadPoints$.subscribe(points => results = points);
	    (<jasmine.Spy>pointService.fetchPoints).and.returnValue(of(mockPoints));
	    spyOnProperty(mapStore, 'groupQueryString').and.returnValue('RE_labs=1234');
	    service.typeaheadInput$.next(query);
	    
	    tick(300);
	    expect(pointService.fetchPoints).toHaveBeenCalledWith(
		mockDataset, 'articles,authors,teams', '20', null, null,
		query, 'RE_labs=1234'
	    );
	    expect(service.searchLoading).toBe(false);
	    expect(results).toEqual(mockPoints);
	}));
    });

    describe('selectSearchResult', () => {
	it('should set selectedPoint on mapStore', () => {
	    const spy = spyOn(mapStore, 'setSelectedPoint');
	    
	    service.selectSearchResult(mockPoints[0]);
	    
	    expect(spy).toHaveBeenCalledWith(mockPoints[0], true);
	});
    });

    describe('selectAllSearchResults', () => {
	it('should push an array with all typeahead results', () => {
	    service.typeaheadResults = mockPoints;
	    service.typeaheadInput = 'generic query';
	    const searchSpy = spyOn(mapStore, 'setSearchPoints');
	    
	    service.selectAllSearchResults();
	    
	    expect(searchSpy).toHaveBeenCalledWith(
		mockPoints, 'Search results for generic query'
	    );
	});
    });

    describe('getNeighbors', () => {
	it('should push neighbors to searchPoints$', () => {
	    dsStore.setDatasets([mockDataset]);
	    dsStore.setDataset(mockDataset.key, mockDataset.version);
	    const searchSpy = spyOn(mapStore, 'setSearchPoints');
	    const selectSpy = spyOn(mapStore, 'setSelectedPoint');
	    (<jasmine.Spy>pointService.getNeighbors).and.returnValue(of(mockPoints));
	    
	    service.getNeighbors(mockPoints[0], 'articles');

	    expect(pointService.getNeighbors).toHaveBeenCalledWith(
		mockDataset, mockPoints[0], 'articles'
	    );
	    expect(selectSpy).toHaveBeenCalledWith(mockPoints[0]);
	    expect(searchSpy).toHaveBeenCalledWith(
		mockPoints, 'Nearest articles for ' + mockPoints[0].label
	    );
	});
    });

    describe('resetService', () => {
	it('should clear the cache and typeahead', () => {
	    service.typeaheadInput$.next('some query');
	    service.typeaheadResults = mockPoints;
	    service._clusterCache['ll_clusters'] = mockPoints;
	    
	    service.resetService();
	    
	    service.typeaheadInput$.subscribe(val => expect(val).toEqual(''));
	    expect(service.typeaheadResults).toEqual([]);
	    expect(service._clusterCache).toEqual({});
	});
    });

    
    describe('_getClusters', () => {
	beforeEach(() => {
	    dsStore.setDatasets([mockDataset]);
	    dsStore.setDataset(mockDataset.key, mockDataset.version);
	    (<jasmine.Spy>pointService.fetchPoints).and.returnValue(of(mockPoints));
	});

	it('should store cluster results in cache', () => {
	    service._getClusters(1);
	    expect(pointService.fetchPoints).toHaveBeenCalledWith(
		mockDataset, 'hl_clusters', '20', null, null, null
	    );
	    expect(service._clusterCache['hl_clusters']).toEqual(mockPoints);
	    service._clusterCache['hl_clusters'].forEach(
		p => expect(p.is_cluster).toBe(true)
	    );
	    
	    (<jasmine.Spy>pointService.fetchPoints).calls.reset();
	    service._getClusters(2);
	    expect((<jasmine.Spy>pointService.fetchPoints).calls.any()).toBe(false);
	});
	
	it('should send a request to fetch clusters based on zoom level', () => {
	    service._getClusters(1);
	    expect(pointService.fetchPoints).toHaveBeenCalledWith(
		mockDataset, 'hl_clusters', '20', null, null, null
	    );
	    
	    service._getClusters(3);
	    expect(pointService.fetchPoints).toHaveBeenCalledWith(
		mockDataset, 'ml_clusters', '32', null, null, null
	    );
	    
	    service._getClusters(10);
	    expect(pointService.fetchPoints).toHaveBeenCalledWith(
		mockDataset, 'll_clusters', '128', null, null, null
	    );
	    
	    service._getClusters(30);
	    expect(pointService.fetchPoints).toHaveBeenCalledWith(
		mockDataset, 'vll_clusters', '512', null, null, null
	    );
	    
	    (<jasmine.Spy>pointService.fetchPoints).calls.reset();
	    service._getClusters(64);
	    expect((<jasmine.Spy>pointService.fetchPoints).calls.any()).toBe(false);
	});
	
	it('should add cluster points to mapStore', () => {
	    const spy = spyOn(mapStore, 'setClusters');
	    service._getClusters(1);
	    expect(pointService.fetchPoints).toHaveBeenCalledWith(
		mockDataset, 'hl_clusters', '20', null, null, null
	    );
	    expect(spy).toHaveBeenCalledWith(mockPoints);
	});
    });

    describe('requestGroupTile', () => {
	it('should return empty dict if no layers', () => {
	    service.requestGroupTile(group, 3, 2, 7, 5, 8, []).subscribe(
		t => expect(t).toEqual({})
	    );
	    expect((<jasmine.Spy>pointService.requestGroupTile).calls.any()).toBe(false);
	});
	
	it('should request group tiling', () => {
	    const layers = [{ label: 'Authors', key: 'authors' },
			    { label: 'Articles', key: 'articles' }] as Nature[];
	    service.requestGroupTile(group, 3, 2, 7, 5, 8, layers);
	    expect(pointService.requestGroupTile).toHaveBeenCalledWith(
		group, 3, 2, 7, 5, 8, layers
	    );
	});
    });

    describe('displayGroup', () => {
	it('should do nothing if no filters', () => {
	    service.displayGroup([]);
	    expect((<jasmine.Spy>pointService.getDatasetGroup).calls.any()).toBe(false);
	});

	it('should get group from pointService and add it to mapStore', () => {
	    dsStore.setDatasets([mockDataset]);
	    dsStore.setDataset(mockDataset.key, mockDataset.version);
	    (<jasmine.Spy>pointService.getDatasetGroup).and.returnValue(of(group));
	    const spy = spyOn(mapStore, 'setGroup');
	    const filters = [{ field: 'labs', value: 'CNRS', bitmap_index: 14,
			       rank: 23786, location: '' }];
	    
	    service.displayGroup(filters);
	    
	    expect(pointService.getDatasetGroup).toHaveBeenCalledWith(mockDataset, filters);
	    expect(spy).toHaveBeenCalledWith([group], filters);
	});
  });

    describe('displayGroupForPoint', () => {
	it('should get group from pointService and add it to mapStore', () => {
	    dsStore.setDatasets([mockDataset]);
	    dsStore.setDataset(mockDataset.key, mockDataset.version);
	    (<jasmine.Spy>pointService.getGroupByReference).and.returnValue(of(group));
	    const spy = spyOn(mapStore, 'setGroup');
	    const groupFilter = { label: 'Lab', key: 'labs',
				  type: 'ID', multi: true } as GroupFilter;
	    service.displayGroupForPoint(groupFilter, mockPoints[1]);
	    
	    expect(pointService.getGroupByReference).toHaveBeenCalledWith(
		mockDataset, groupFilter, mockPoints[1].rank
	    );
	    expect(spy).toHaveBeenCalledWith(
		[group],
		[{ field: groupFilter.key, value: mockPoints[1].label,
		   rank: mockPoints[1].rank, bitmap_index: undefined,
		   location: undefined }]);
	});
    });
});

const fakePoints = {
    words: [{ id: '1', score: 10, position: [1.0, 2.0], nature: 'words' },
	    { id: '2', score: 5, position: [-5.2, -3.0], nature: 'words' }] as Point[],
    articles: [{ id: '3', score: 10, position: [1.0, 2.0], nature: 'articles' },
	       { id: '4', score: 5, position: [-5.2, -3.0], nature: 'articles' }] as Point[],
    authors: [{ id: '5', score: 10, position: [1.0, 2.0], nature: 'authors' },
	      { id: '6', score: 5, position: [-5.2, -3.0], nature: 'authors' }] as Point[],
    teams: [{ id: '7', score: 10, position: [1.0, 2.0], nature: 'teams' },
	    { id: '8', score: 5, position: [-5.2, -3.0], nature: 'teams' }] as Point[]
};

const fakeFetchPoints = (_dataset: Dataset, nature: string, _limit: string,
			 _xrange: number[], _yrange: number[],
			 _query: string) => {
			     return of(fakePoints[nature]);
			 };
