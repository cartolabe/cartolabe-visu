import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatListModule } from '@angular/material/list';
import { of } from 'rxjs';
import { ActivatedRoute, ActivatedRouteStub } from 'src/testing/router.stubs';
import { DatasetServiceStub } from 'src/testing/services.stubs';
import { AboutComponent } from './about.component';
import { Dataset, Nature } from 'src/app/datasets/models';
import { DatasetService } from 'src/app/datasets/services/dataset.service';
import { DatasetsStore } from 'src/app/datasets/store';

describe('AboutComponent', () => {
  let component: AboutComponent;
  let fixture: ComponentFixture<AboutComponent>;
  let activatedRoute: ActivatedRouteStub;
  let datasetService: DatasetService;
  let dsStore: DatasetsStore;
  const natures = [
    { label: 'Articles', key: 'articles' },
    { label: 'Authors', key: 'authors' },
    { label: 'Teams', key: 'teams' },
    { label: 'Labs', key: 'labs' }
  ] as Nature[];
  const datasets = [{ id: 1, label: 'Inria', key: 'inria', description: '', natures: natures },
  { id: 2, label: 'LRI', key: 'lri', description: '', natures: [] }] as Dataset[];

  beforeEach(() => {
    activatedRoute = new ActivatedRouteStub();
    activatedRoute.testFragment = datasets[1].key;
    TestBed.configureTestingModule({
      imports: [MatListModule],
      declarations: [AboutComponent],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRoute },
        { provide: DatasetService, useClass: DatasetServiceStub }
      ]
    }).compileComponents();
    datasetService = TestBed.get(DatasetService);
    dsStore = TestBed.get(DatasetsStore);
    dsStore.setDatasets(datasets);

    fixture = TestBed.createComponent(AboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get the datasets from the store', () => {
    let d = null;
    component.datasets$.subscribe(ds => d = ds);
    expect(d).toEqual(datasets);
  });

  it('should select the dataset from the route fragment', () => {
    expect(component.selectedDataset).toEqual(datasets[1]);
  });

  describe('selectDataset', () => {
    beforeEach(() => {
      (<jasmine.Spy>datasetService.getDatasetStats).calls.reset();
    });

  it('should get the stats from the datasetService', () => {
      component.selectDataset(datasets[0]);
      expect(datasetService.getDatasetStats).toHaveBeenCalledWith(
	    datasets[0].key, datasets[0].current
      );
  });

    it('should compute the total from the stats', () => {
      const stats = { articles: 12, authors: 15, teams: 2 };
      (<jasmine.Spy>datasetService.getDatasetStats).and.returnValue(of(stats));
      component.selectDataset(datasets[0]);
      expect(component.stats).toBe(stats);
      expect(component.total).toBe(29);
    });
  });
});

