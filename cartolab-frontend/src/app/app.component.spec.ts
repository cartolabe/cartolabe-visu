import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

describe('AppComponent', () => {
    let component;
    let fixture;

    beforeEach(async () => {
	TestBed.configureTestingModule({
	    imports: [
		RouterTestingModule,
		BrowserAnimationsModule
	    ],
	    declarations: [
		AppComponent,
	    ],
	}).compileComponents();
    });

    beforeEach(() => {
	fixture = TestBed.createComponent(AppComponent);
	component = fixture.componentInstance;
	fixture.detectChanges();
    });

    it('should create', () => {
	expect(component).toBeTruthy();
    });
    
    it('should have the viewRef as attribute', () => {
	expect(component.vcRef).toBeTruthy();
    });

});

