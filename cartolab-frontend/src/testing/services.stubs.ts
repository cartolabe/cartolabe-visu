import { BehaviorSubject, of } from 'rxjs';
import { PointService, MapService } from 'src/app/map/services';
import { User } from 'src/app/auth/models';
import { AuthService } from 'src/app/auth/services/auth.service';
import { DatasetService } from 'src/app/datasets/services/dataset.service';


export class MapServiceStub implements Partial<MapService> {
  clearGroup = jasmine.createSpy('clearGroup');
  displayGroup = jasmine.createSpy('displayGroup');
  requestGroupTile = jasmine.createSpy('requestGroupTile');
  displayGroupForPoint = jasmine.createSpy('displayGroupForPoint');
  updateViewBox = jasmine.createSpy('updateViewBox');
  setSelectedPoint = jasmine.createSpy('setSelectedPoint');
  getNeighbors = jasmine.createSpy('getNeighbors');
  searchInBounds = jasmine.createSpy('searchInBounds');
  resetService = jasmine.createSpy('resetService');
}

export class PointServiceStub implements Partial<PointService> {
  getNeighbors = jasmine.createSpy('getNeighbors');
  fetchPoints = jasmine.createSpy('fetchPoints');
  requestGroupTile = jasmine.createSpy('requestGroupTile');
  getGroupByReference = jasmine.createSpy('getGroupByReference');
  getDatasetGroup = jasmine.createSpy('getDatasetGroup');
}

export class DatasetServiceStub implements Partial<DatasetService> {
  getDataset = jasmine.createSpy('getDataset');
  getDatasets = jasmine.createSpy('getDatasets');
  searchFilterValues = jasmine.createSpy('searchFilterValues');
  getDatasetStats = jasmine.createSpy('getDatasetStats').and.returnValue(of({}));
}

export class AuthServiceStub implements Partial<AuthService> {
  loggedUser = new BehaviorSubject<User>(null);
  login = jasmine.createSpy('login');
  logout = jasmine.createSpy('logout');
  isLoggedIn = jasmine.createSpy('isLoggedIn');
}
