from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from datasets.bitmaps.views import (
    DatasetGroupDetailView, FilterValueList, DatasetGroupByReferenceView
)
from datasets.views import DatasetList, DatasetDetailView, DatasetStatsView

urlpatterns = [
    path('', DatasetList.as_view(), name='dataset-list'),
    path('<int:pk>/', DatasetDetailView.as_view(), name='dataset-detail'),
    path('<str:key>/', DatasetDetailView.as_view(), name='dataset-detail-key'),
    path('<str:key>/<str:version>/group/',
         DatasetGroupDetailView.as_view(), name='dataset-group-detail'),
    path('<str:key>/<str:version>/groupbyreference/',
         DatasetGroupByReferenceView.as_view(),
         name='dataset-group-by-reference'),
    path('<str:key>/<str:version>/stats/', DatasetStatsView.as_view(),
         name='dataset-stats'),
    path('<str:key>/<str:version>/filtervalues/',
         FilterValueList.as_view(), name='filter-values'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
