from django.urls import path

from datasets.bitmaps.views import GroupTileView

urlpatterns = [
    path('<uuid:uuid>/<int:z>/<int:x>/<int:y>/<int:xend>/<int:yend>/',
         GroupTileView.as_view(), name='group-tile'),
]
