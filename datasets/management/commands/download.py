import os
from download import download

from django.conf import settings
from django.core.management.base import BaseCommand

from datasets.models import Dataset


class Command(BaseCommand):
    help = 'Create the tiles for a dataset.'

    def add_arguments(self, parser):
        parser.add_argument('--dataset_key',
                            help='The dataset key to download.')
        parser.add_argument('--file_name',
                            help='The file name of the dump.')
        parser.add_argument('--file_url',
                            help='The URL of the dump file to download.')
        parser.add_argument('--dump_dir',
                            help='Path for dumps directory.')
        parser.add_argument('--dataset_version',
                            help='The dataset version to process.')

    def handle(self, *args, **options):
        dataset_key = options['dataset_key']
        file_name = options['file_name']
        file_url = options['file_url']
        dump_dir = options['dump_dir']
        version = options['dataset_version']

        if dataset_key is None:
            dataset_key = self.get_dataset_key()
        if file_name is None:
            file_name = "export.feather"
        if dump_dir is None:
            dump_dir = settings.DATASETS_DUMP_DIR

        dataset = Dataset.objects.get(key=dataset_key)

        if version is None:
            version = dataset.version

        dump_file_dir = os.path.join(dump_dir, dataset_key,
                                     version)
        print(f"Make directory {dump_file_dir}")
        try:
            os.makedirs(dump_file_dir)
        except(FileExistsError):
            print(f"The directory {dump_file_dir} already exists!")

        filepath = os.path.join(dump_file_dir, file_name)

        if os.path.exists(filepath):
            print(f"The file {filepath} already exists. No need to download.")
        else:
            if file_url is not None:
                dataset.url = file_url

            url = dataset.url

            print(f"The file {filepath} does not exist. Downloading file from"
                  f" URL {url}...")

            if url is None:
                print(
                    "Please provide URL for the dataset in the fixture file."
                )
            else:
                download(url, filepath, kind='file',
                         progressbar=True, replace=False)
            dataset.save()

    def get_dataset_key(self):
        keys = list(Dataset.objects.all().values_list('key', flat=True))
        return self.get_input_data('Dataset to download ({}): '.format(', '.join(keys)))

    def get_input_data(self, message, default=None):
        """
        Override this method if you want to customize data inputs or
        validation exceptions.
        """
        raw_value = input(message)
        if default and raw_value == '':
            raw_value = default

        return raw_value
