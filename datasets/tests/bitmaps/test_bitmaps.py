import os
import pickle
from unittest.mock import patch, MagicMock, ANY

import numpy as np
import pandas as pd
from django.conf import settings
from django.test import TestCase
from pyfakefs import fake_filesystem_unittest
from pyroaring import BitMap

from datasets.bitmaps.bitmaps import (
    BitmapStore, GROUP_FILTER_INDEX_SUFFIX, CoordBitmapStore,
    combine_bitmaps, group_tiling_task, BitmapFilesManager,
    get_pandas_df_from_bitmap
)
from datasets.bitmaps.models import GroupFilter, FilterValue, Group
from datasets.models import Dataset


def notqdm(it, *a, **k):
    return it


class BitmapStoreTestCase(TestCase):
    def setUp(self):
        self.dataset = Dataset.objects.create(label="inria", key="inria",
                                              version="1.0.0")
        self.f1 = GroupFilter.objects.create(
            label='Lab', key='lab', multi=True, dataset=self.dataset,
            type=GroupFilter.REF
        )
        self.f2 = GroupFilter.objects.create(
            label='Year', key='year', multi=True, dataset=self.dataset,
            type=GroupFilter.INT
        )
        self.f3 = GroupFilter.objects.create(
            label='Category', key='cat', dataset=self.dataset,
            type=GroupFilter.STR
        )
        self.filters = [self.f1, self.f2, self.f3]
        self.output_dir = os.path.join(
            settings.BITMAPS_DIR, self.dataset.key, self.dataset.version
        )
        self.store = BitmapStore(self.dataset, self.filters, self.output_dir)

    def test_add_field_value(self):
        """
        method should add id to the bitmap for field/value
        """
        self.store.add_field_value(34, 'year', 2012)
        self.store.add_field_value(35, 'year', 2012)
        self.store.add_field_value(34, 'lab', 2)
        self.store.add_field_value(35, 'lab', 3)
        self.store.add_field_value(34, 'cat', 'yellow')
        self.store.add_field_value(35, 'cat', 'blue')

        self.assertDictEqual(
            self.store.index,
            {'year': {2012: BitMap([34, 35])},
             'lab': {2: BitMap([34]), 3: BitMap([35])},
             'cat': {'yellow': BitMap([34]), 'blue': BitMap([35])}
             }
        )

    def test_format_filter_value_ref(self):
        """
        method should create a FilterValue with value as string and rank
        """
        def elmt_field_getter(x, y): return 'CNRS'
        value = 2
        filter_value = self.store._format_filter_value(
            self.f1, value, 0, elmt_field_getter
        )
        self.assertEquals(filter_value, self.create_filter_value(
            self.f1.key, 'CNRS', 0, value)
        )

    def test_format_filter_value_int(self):
        """
        method should create a FilterValue with value as string and rank
        """
        labels = MagicMock()
        value = '2000'
        filter_value = self.store._format_filter_value(
            self.f2, value, 0, labels
        )
        self.assertEquals(filter_value,
                          self.create_filter_value(
                              self.f2.key, int(value), 0)
                          )
        labels.assert_not_called()

    def test_format_filter_value_str(self):
        """
        method should create a FilterValue with value as string and rank
        """
        labels = MagicMock()
        value = 'INRIA'
        filter_value = self.store._format_filter_value(
            self.f3, value, 0, labels
        )
        self.assertEquals(
            filter_value, self.create_filter_value(self.f3.key, value, 0)
        )
        labels.assert_not_called()

    @patch('datasets.bitmaps.bitmaps.tqdm', notqdm)
    def test_gen_filter_values(self):
        self.store.add_field_value(34, 'year', 2012)
        self.store.add_field_value(34, 'lab', 2)
        self.store.add_field_value(35, 'lab', 3)
        self.store.add_field_value(35, 'cat', 'yellow')
        values = list(self.store._gen_filter_values(
            lambda x, y: 'CNRS' if x == 2 else 'INRIA')
        )

        self.assertEqual(len(values), 4)
        self.assertEqual(values[0], self.create_filter_value(
            'lab', 'CNRS', 0, 2).to_dict(True)
        )
        self.assertEqual(values[1], self.create_filter_value(
            'lab', 'INRIA', 1, 3).to_dict(True)
        )
        self.assertEqual(values[2], self.create_filter_value(
            'year', 2012, 2).to_dict(True)
        )
        self.assertEqual(values[3], self.create_filter_value(
            'cat', 'yellow', 3).to_dict(True)
        )

    @patch('datasets.bitmaps.bitmaps.tqdm', notqdm)
    def test_gen_filter_values_with_min(self):
        self.store.add_field_value(34, 'year', 2012)
        self.store.add_field_value(35, 'year', 2012)
        self.store.add_field_value(34, 'lab', 2)
        self.store.add_field_value(35, 'lab', 3)
        self.store.filters[0].min_size = 2
        values = list(self.store._gen_filter_values(
            lambda x: 'CNRS' if x == 2 else 'INRIA')
        )

        self.assertEqual(len(values), 1)
        self.assertEqual(values[0],
                         self.create_filter_value(
                             'year', 2012, 0).to_dict(True)
                         )

    def create_filter_value(self, field, value, bitmap, rank=None):
        if rank is not None:
            return FilterValue(
                field=field, value=value, bitmap_index=bitmap, rank=rank,
                meta={
                    'index': self.dataset.key + "-" + self.dataset.version +
                    GROUP_FILTER_INDEX_SUFFIX
                }
            )
        return FilterValue(
            field=field, value=value, bitmap_index=bitmap,
            meta={
                'index': self.dataset.key + "-" + self.dataset.version +
                GROUP_FILTER_INDEX_SUFFIX}
        )

    @patch('datasets.bitmaps.bitmaps.BitmapFilesManager')
    def test_store_bitmaps(self, mock_bfm_class):
        """
        store_bitmaps should create a new BitmapFilesManager and call
        store_bitmaps on it
        """
        mock_bfm = MagicMock()
        mock_bfm_class.return_value = mock_bfm
        bitmaps = [BitMap([33])]
        self.store.bitmaps = bitmaps
        self.store._store_bitmaps()

        mock_bfm.store_bitmaps.assert_called_with(bitmaps)


class BitmapFilesManagerTestCase(TestCase, fake_filesystem_unittest.TestCase):
    def setUp(self):
        self.output_dir = os.path.join(settings.BITMAPS_DIR, 'testds')
        self.manager = BitmapFilesManager(self.output_dir)
        self.setUpPyfakefs()
        self.fs.create_dir(self.output_dir)

    def test_gen_filename(self):
        filenames = self.manager._gen_filename()
        self.assertEquals(next(filenames), os.path.join(
            self.output_dir, 'AA', 'bitmaps_00.pickle')
        )
        self.assertEquals(next(filenames), os.path.join(
            self.output_dir, 'AA', 'bitmaps_01.pickle')
        )
        self.assertEquals(next(filenames), os.path.join(
            self.output_dir, 'AA', 'bitmaps_02.pickle')
        )
        self.assertTrue(os.path.isdir(os.path.join(self.output_dir, 'AA')))

    def test_get_filename_from_index(self):
        bitmaps_per_dir = (
            self.manager.MAX_BITMAPS_PER_FILE *
            self.manager.MAX_FILES_PER_SUBDIR
        )

        filename, sub_idx = self.manager._get_filename_from_index(
            bitmaps_per_dir + 1
        )
        self.assertEquals(
            filename, os.path.join(self.output_dir, 'BB', 'bitmaps_00.pickle')
        )
        self.assertEquals(sub_idx, 1)

        filename, sub_idx = self.manager._get_filename_from_index(
            bitmaps_per_dir - 2
        )
        self.assertEquals(
            filename,
            os.path.join(self.output_dir, 'AA',
                         'bitmaps_{:02d}.pickle'.format(
                             self.manager.MAX_FILES_PER_SUBDIR - 1))
        )
        self.assertEquals(sub_idx, self.manager.MAX_BITMAPS_PER_FILE - 2)

    def test_get_bitmap(self):
        filename = os.path.join(self.output_dir, 'AA', 'bitmaps_00.pickle')
        bitmaps = [BitMap([13, 24])]
        self.fs.create_file(filename, contents=pickle.dumps(bitmaps))

        bm = self.manager.get_bitmap(0)
        self.assertEquals(bm, bitmaps[0])
        self.assertDictEqual(self.manager.cache, {filename: bitmaps})

        filename2 = os.path.join(self.output_dir, 'BB', 'bitmaps_03.pickle')
        bitmaps2 = [BitMap([13, 24]), BitMap([12, 25]), BitMap([45, 52])]
        self.fs.create_file(filename2, contents=pickle.dumps(bitmaps2))
        bpd = (
            self.manager.MAX_BITMAPS_PER_FILE *
            self.manager.MAX_FILES_PER_SUBDIR
        )
        idx = bpd + 3 * self.manager.MAX_BITMAPS_PER_FILE + 2

        bm = self.manager.get_bitmap(idx)
        self.assertEquals(bm, bitmaps2[2])
        self.assertDictEqual(self.manager.cache, {
                             filename: bitmaps, filename2: bitmaps2})

        os.remove(filename2)
        bm = self.manager.get_bitmap(idx - 1)
        self.assertEquals(bm, bitmaps2[1])
        self.assertDictEqual(self.manager.cache, {
                             filename: bitmaps, filename2: bitmaps2})

    def test_store_bitmaps(self):
        """
        store_bitmaps should store bitmap list in separate files
        :return:
        """
        bitmaps = [BitMap([x])
                   for x in range(self.manager.MAX_BITMAPS_PER_FILE)]
        bitmaps.append(BitMap([10]))

        self.manager.store_bitmaps(bitmaps)
        self.assertTrue(os.path.isfile(os.path.join(
            self.output_dir, 'AA', 'bitmaps_00.pickle')))
        self.assertTrue(os.path.isfile(os.path.join(
            self.output_dir, 'AA', 'bitmaps_01.pickle')))


class CoordBitmapStoreTestCase(TestCase):
    def setUp(self):
        self.store = CoordBitmapStore()

    @patch('datasets.bitmaps.bitmaps.tqdm')
    def test_create_store(self, mock_tqdm):
        """
        create_store should create a list of bitmaps, each bitmap
        corresponding to one tile of the map at zoom level z
        and containing the ids of points on this tile.
        For zoom = 1, Tiles:
        p0  |  p1, p2
        ____|________
        p3  |

        For zoom = 2, Tiles:
           |  |p1|
        ------------
         p0|  |  |p2
        ------------
           |  |  |
        ------------
         p3|  |  |
        """
        df = pd.DataFrame([{'x_pos': -10.2, 'y_pos': 5.2},
                           {'x_pos': 5.1, 'y_pos': 14.2},
                           {'x_pos': 13.2, 'y_pos': 7.2},
                           {'x_pos': -8.3, 'y_pos': -8.3}])
        coords_range = [-15, 15]
        self.store.create_store(df, coords_range, 1)

        self.assertEqual(self.store.zoom, 1)
        self.assertEqual(self.store.coords_range, coords_range)
        self.assertEquals(self.store.bitmaps[0], BitMap([0]))
        self.assertEquals(self.store.bitmaps[1], BitMap([3]))
        self.assertEquals(self.store.bitmaps[2], BitMap([1, 2]))

        self.store.create_store(df, coords_range, 2)
        self.assertEqual(self.store.zoom, 2)
        self.assertEqual(self.store.coords_range, coords_range)
        self.assertEqual(len(self.store.bitmaps), 16)
        e = BitMap()
        self.assertEquals(self.store.bitmaps,
                          [e, BitMap([0]), e, BitMap([3]), e, e, e, e,
                           BitMap([1]), e, e, e, e, BitMap([2]), e, e])

    def test_get_contained_coords_same_zoom(self):
        self.store.zoom = 8
        (xmin, ymin), (xmax, ymax) = self.store._get_contained_coords(
            2, 54, 8, 36, 112
        )
        self.assertEqual(xmin, 2)
        self.assertEqual(ymin, 54)
        self.assertEqual(xmax, 38)
        self.assertEqual(ymax, 166)

    def test_get_contained_coords_smaller_zoom(self):
        self.store.zoom = 7
        (x, y, z, xrange, yrange) = (6, 2, 3, 2, 3)
        (xmin, ymin), (xmax, ymax) = self.store._get_contained_coords(
            x, y, z, xrange, yrange
        )
        self.assertEqual(xmin, x * pow(2, 4))
        self.assertEqual(ymin, y * pow(2, 4))
        self.assertEqual(xmax, (x + xrange) * pow(2, 4) - 1)
        self.assertEqual(ymax, (y + yrange) * pow(2, 4) - 1)

    def test_get_contained_coords_greater_zoom(self):
        self.store.zoom = 3
        (x, y, z, xrange, yrange) = (14, 2, 6, 33, 14)
        (xmin, ymin), (xmax, ymax) = self.store._get_contained_coords(
            x, y, z, xrange, yrange
        )
        self.assertEqual(xmin, x // pow(2, 3))
        self.assertEqual(ymin, y // pow(2, 3))
        self.assertEqual(xmax, (x + xrange - 1) // pow(2, 3))
        self.assertEqual(ymax, (y + yrange - 1) // pow(2, 3))

    def test_get_bitmap_for_tiles(self):
        self.store.zoom = 3
        self.store.bitmaps = [BitMap([x]) for x in range(64)]

        (x, y, z, xrange, yrange) = (14, 2, 6, 33, 14)
        (xmin, ymin), (xmax, ymax) = (1, 0), (5, 1)
        n = pow(2, 3)
        ids = [i * n + j for i in range(xmin, xmax + 1)
               for j in range(ymin, ymax + 1)]
        bm = self.store.get_bitmap_for_tiles(x, y, z, xrange, yrange)
        self.assertEquals(bm, BitMap(ids))


class BitMapsTestCase(TestCase, fake_filesystem_unittest.TestCase):

    def setUp(self):
        self.dataset = Dataset.objects.create(
            label="inria", key="inria", version="1.0.0",
            metadata="{\"coords_range\": [-21,21]}"
        )
        self.group = Group.objects.create(
            dataset=self.dataset, bitmaps='34&400')
        self.setUpPyfakefs()

    def test_combine_bitmaps(self):
        manager = MagicMock()
        manager.get_bitmap = lambda x: bms[x]
        bms = [BitMap([3, 27, 42]), BitMap([3, 18]),
               BitMap([15, 27, 65]), BitMap([18, 42, 65])]
        bm = combine_bitmaps(manager, '0|2&1', ['&', '|'])
        self.assertEquals(bm, BitMap([3]))
        bms = [BitMap([3, 27, 42]), BitMap([3, 18]),
               BitMap([15, 27, 65]), BitMap([18, 42, 65])]
        bm = combine_bitmaps(manager, '0&1|3', ['&', '|'])
        self.assertEquals(bm, BitMap([3, 42]))
        bms = [BitMap([3, 27, 42]), BitMap([3, 18]),
               BitMap([15, 27, 65]), BitMap([18, 42, 65])]
        bm = combine_bitmaps(manager, '0&1|3', ['|', '&'])
        self.assertEquals(bm, BitMap([3, 18, 42, 65]))
        bms = [BitMap([3, 27, 42]), BitMap([3, 18]),
               BitMap([15, 27, 65]), BitMap([18, 42, 65])]
        bm = combine_bitmaps(manager, '0&3|2&1', ['|', '&'])
        self.assertEquals(bm, BitMap([42]))

    @patch('datasets.bitmaps.bitmaps.get_bitmap_for_coords')
    @patch('datasets.bitmaps.bitmaps.get_bitmap_from_group')
    def test_group_tiling_task_when_empty(self, mock_get_bitmaps,
                                          mock_get_bm_coords):
        """
        When no points correspond to the group to be displayed, tiling task
        should just return the tiles param.
        """
        mock_get_bitmaps.return_value = BitMap([12])
        mock_get_bm_coords.return_value = BitMap([5])
        ones = np.ones((2, 5))
        layers = {'articles': ones, 'authors': ones}
        res = group_tiling_task(self.group.uuid, 0, 2, 5, layers)

        self.assertEquals(len(res), 2)
        self.assertTrue(np.all(res['articles'] == ones))
        self.assertTrue(np.all(res['authors'] == ones))
        mock_get_bitmaps.assert_called_with(self.group)
        mock_get_bm_coords.assert_called_with(
            self.group, 0, 2, 5, ones.shape[0], ones.shape[1])

    @patch('datasets.bitmaps.bitmaps.create_tiles_for_x_y')
    @patch('datasets.bitmaps.bitmaps.get_pandas_df_from_bitmap')
    @patch('datasets.bitmaps.bitmaps.get_bitmap_for_coords')
    @patch('datasets.bitmaps.bitmaps.get_bitmap_from_group')
    def test_group_tiling_task(self, mock_get_bitmaps, mock_get_bm_coords,
                               mock_get_df, mock_create_tiles):
        """
        Tiling task should get bitmaps for points, then get the DataFrame of
        point positions, then call tiling method
        to create tiles and return an np.array of ones.
        """
        mock_get_bitmaps.return_value = BitMap([5, 10, 12, 14])
        mock_get_bm_coords.return_value = BitMap([10, 12])
        mock_df = pd.DataFrame(
            [{'nature': 'articles', 'x_pos': 2, 'y_pos': 3}])
        mock_get_df.return_value = mock_df

        ones = np.ones((2, 5))
        layers = {'articles': ones, 'authors': ones}
        res = group_tiling_task(self.group.uuid, 0, 2, 5, layers)

        self.assertEquals(len(res), 2)
        self.assertTrue(np.all(res['articles'] == ones))
        self.assertTrue(np.all(res['authors'] == ones))
        mock_get_bitmaps.assert_called_with(self.group)
        mock_get_bm_coords.assert_called_with(
            self.group, 0, 2, 5, ones.shape[0], ones.shape[1])
        mock_get_df.assert_called_with(self.group, BitMap([10, 12]))

        zoom_dir = os.path.join(
            settings.STATIC_ROOT,
            'grouptiles/{}/{}'.format(self.group.uuid, 'articles'), '5'
        )
        mock_create_tiles.assert_called_with(
            ANY, 256, 5, 0, 2, ones.shape[0], ones.shape[1], zoom_dir,
            [-21, 21]
        )
        self.assertTrue(os.path.isdir(zoom_dir))

    @patch('datasets.bitmaps.bitmaps.cache')
    def test_get_pandas_df_from_bitmap_uses_cache(self, mock_cache):
        pos = [{'x_pos': 1, 'y_pos': 2}, {'x_pos': 3, 'y_pos': 4}]
        df = pd.DataFrame(pos)
        mock_cache.get.return_value = df
        bm = BitMap([1])

        res = get_pandas_df_from_bitmap(self.group, bm)
        self.assertEquals(res.to_dict('records'), pos[1:])
        mock_cache.get.assert_called_with(
            self.dataset.key + self.dataset.version
        )
        mock_cache.set.assert_not_called()

    @patch('datasets.bitmaps.bitmaps.cache')
    @patch('datasets.bitmaps.bitmaps.pd.read_feather')
    def test_get_pandas_df_from_bitmap_sets_cache(self, mock_read_feather,
                                                  mock_cache):
        pos = [{'index': 0, 'x_pos': 1, 'y_pos': 2},
               {'index': 1, 'x_pos': 3, 'y_pos': 4}]
        df = pd.DataFrame(pos)
        mock_cache.get.return_value = None
        mock_read_feather.return_value = df
        bm = BitMap([1])

        res = get_pandas_df_from_bitmap(self.group, bm)
        del pos[1]['index']
        self.assertEquals(res.to_dict('records'), pos[1:])
        mock_cache.get.assert_called_with(
            self.dataset.key+self.dataset.version
        )
        mock_cache.set.assert_called_with(
            self.dataset.key+self.dataset.version, ANY
        )
