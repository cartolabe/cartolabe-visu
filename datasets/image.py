import os
# from concurrent.futures import ThreadPoolExecutor

import numpy as np

import skimage as ski
import skimage.transform as skt
import skimage.filters as skf
import skimage.exposure as ske

from PIL import Image
try:
    from fast_histogram import histogram2d
except ModuleNotFoundError:
    from numpy import histogram2d

# Disable DecompressionBombWarning from PIL.
# see https://github.com/zimeon/iiif/issues/11
Image.MAX_IMAGE_PIXELS = 1000000000


def create_tiles_for_zoom(df, tile_size, max_zoom, target_directory,
                          coords_range, pbar=None, dump_images=False):
    tiler = None
    # Start by creating the overview images 0 to 5 in one go
    for zoom in reversed(range(min(max_zoom, 6))):
        zoom_dir = os.path.join(target_directory, str(zoom))
        os.mkdir(zoom_dir)
        num_tiles = 2**zoom
        img_size = num_tiles * tile_size
        if tiler is None:
            tiler = Tiler(img_size, img_size, coords_range, coords_range)
            tiler.dump_images = dump_images
            tiler.create_image(df)
        else:
            tiler.half_image()
        tiler.crop_image(num_tiles, num_tiles, tile_size, zoom_dir,
                         0, 0, pbar=pbar)
    imax = tiler.max
    if max_zoom <= 6:
        return
    medium_tiles = 2**(max_zoom - 6)
    coords = np.linspace(coords_range[0], coords_range[1], medium_tiles+1)
    for x in range(medium_tiles):
        x_range = (coords[x], coords[x+1])
        for y in range(medium_tiles):
            y_range = (coords[medium_tiles-y-1], coords[medium_tiles-y])
            num_tiles = 32
            img_size = num_tiles * tile_size
            tiler = None
            for zoom in reversed(range(6, max_zoom)):
                zoom_dir = os.path.join(target_directory, str(zoom))
                if not os.path.exists(zoom_dir):
                    os.mkdir(zoom_dir)
                if tiler is None:
                    tiler = Tiler(img_size, img_size, x_range, y_range)
                    tiler.dump_images = dump_images
                    tiler.create_image(df)
                else:
                    num_tiles //= 2
                    tiler.half_image()
                    if tiler.max != 0:
                        tiler.max = imax  # fix intensity scale
                tiler.crop_image(num_tiles, num_tiles, tile_size, zoom_dir,
                                 x * num_tiles, y * num_tiles, pbar=pbar)


class Tiler:
    """
    Class to manage the tiling of an dataset

    Attributes:
        width, height: the size of the np.array created
        x_range, y_range: the data range
    """

    def __init__(self, width, height, x_range, y_range):
        self.width = width
        self.height = height
        self.x_range = x_range
        self.y_range = y_range
        self.image = None
        self.max = None
        self.dump_images = False

    def create_image(self, df):
        """
        Creates the image from the x_pos and y_pos columns in ds
        :return:
        """
        x = df['x_pos'].values
        y = df['y_pos'].values
        histogram = histogram2d(y, x,
                                bins=(self.height, self.width),
                                range=[self.y_range, self.x_range])
        if isinstance(histogram, tuple):  # uses numpy histogram2d
            histogram = histogram[0]
        image = histogram
        self.max = image.max()
        if self.max != 0:  # happens for corners of the image
            image = np.flipud(image)
        self.image = image
        return image

    def half_image(self):
        """
        Shrink the image array in half in both dimensions.
        """
        self.width //= 2
        self.height //= 2
        if self.max == 0:
            self.image = self.image[0:self.width, 0:self.height]
        else:
            self.image = skt.pyramid_reduce(self.image, sigma=0,
                                            channel_axis=None)

    def enhance_image(self):
        """
        Compute an enhanced image from the image array, applying a
        Gaussian blur, an intensity correction, and a rescaling to
        fit the whole image range.
        :return:a 8bit Grey-level image
        """
        if self.max == 0:
            image = self.image
        else:
            image = skf.gaussian(self.image, sigma=1)
            image = np.cbrt(image)
            # image = eq_hist(image)
            # image = np.log2(image+1)
            image = ske.rescale_intensity(image)  # , in_range=(0, self.max))
            # image = ske.equalize_hist(image, mask=(image<0.05))
        image = ski.img_as_ubyte(image)
        image = Image.fromarray(image, "L")
        return image

    def crop_image(self, num_tiles_x, num_tiles_y, tile_size,
                   zoom_dir, x_offset, y_offset,
                   fmt='.png', pbar=None):
        """
        Crops the image with a given number of tiles in x and y dimensions.
        :param num_tiles_x: the number of tiles in the x direction
        :param num_tiles_y: the number of tiles in the y direction
        :param tile_size: the size of the tile (typically 256)
        :param zoom_dir: the directory where the tiles will be store
        :param x_offset: the offset of the first x tile
        :param y_offset: the offset of the first y tile
        :param fmt: image format (png by default)
        :param pbar: the optional progress bar
        """
        image = self.enhance_image()
        if self.dump_images:
            image.save(os.path.join(zoom_dir,
                                    f"full_{x_offset}_{y_offset}{fmt}"))
        for x in range(num_tiles_x):
            x_dir = os.path.join(zoom_dir, str(x_offset + x))
            if not os.path.exists(x_dir):
                os.mkdir(x_dir)
            for y in range(num_tiles_y):
                box = (x * tile_size, y * tile_size,
                       (x + 1) * tile_size, (y + 1) * tile_size)
                _crop_save(image, box,
                           os.path.join(x_dir, str(y_offset + y) + fmt))
                if pbar:
                    pbar.update(1)


def _crop_save(image, box, filename):
    croped_image = image.crop(box)
    croped_image.save(filename)


def _eq_hist(image, nbins=256):
    filteredimage = image[image != 0]
    hist, bins = np.histogram(filteredimage, nbins)
    cdf = hist.cumsum()
    cdf = cdf / float(cdf[-1])
    cdf = np.concatenate(([0], cdf))
    return np.interp(image, bins, cdf).reshape(image.shape)
